package com.ernitl.senjuana.practica2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText num1, num2;
    private TextView Display;
    private RadioButton rbSuma,rbResta, rbDivision,rbMultiplicacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        Display = (TextView) findViewById(R.id.Display);
        rbSuma =(RadioButton)findViewById(R.id.suma);
        rbResta =(RadioButton)findViewById(R.id.resta);
        rbMultiplicacion =(RadioButton)findViewById(R.id.multiplicacion);
        rbDivision =(RadioButton)findViewById(R.id.divi);
    }

    public void calcular (View view){
        String valor1=num1.getText().toString();
        String valor2=num2.getText().toString();
        float nro1=Float.parseFloat(valor1);
        float nro2=Float.parseFloat(valor2);
        double res = 0.0;
        if (rbSuma.isChecked() == true)
            res = nro1 + nro2 ;
        else if (rbResta.isChecked() == true)
            res = nro1 - nro2 ;
        else if (rbDivision.isChecked() == true)
            res = (double) nro1 / nro2 ;
        else if (rbMultiplicacion.isChecked() == true)
            res = nro1 * nro2 ;String resu=String.valueOf(res);
        Display.setText(resu);
    }
}
