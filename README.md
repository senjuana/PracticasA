![Image](http://i.imgur.com/8AWyo3f.png)


# Practicas de Android


## About
Este es un repositorio publico que existe para poder guardar practicas de android.

## Requirements
Supported operating systems:
* Development enviroments: All.

Requirements:
* OpenJDK 8
* Androidsdk
* Android Studio


## Getting the code
	$ git clone https://senjuana@gitlab.com/senjuana/PracticasA.git
	$ cd PracticasA


# Contributing
Si encuentras un bug en el codigo o un issue, por favor notificame de manera privada en
[mi cuenta personal de twitter](https://twitter.com/senjuana).

Este proyecto sigue [code of merit](https://github.com/rosarior/Code-of-Merit). En este repositorio, me importa el codigo,
no opiniones personales o sentimientos. Espero tratar con  adultos.

Antes de enviar un pedazo de codigo por favor verifica que lo que envias funciona, yo no soy la persona que resolvera tus problemas con tu codigo.

# FAQ

* **Cual es el punto de compartir tu codigo?**

    Mi unico interes es el de poder compartir mi projectos como  herramienta de aprendizaje.

* **Puedo utilizar este codigo en mis proyectos?**

    Siempre y cuando sigas los parametros de la licencia cualquiera puede utilizar o modiificar las implementaciones de este repositorio para cualquier proyecto personal.




