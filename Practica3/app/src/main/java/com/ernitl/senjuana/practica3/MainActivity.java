package com.ernitl.senjuana.practica3;

import android.graphics.Region;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    //declaracion de los elementos
    private EditText Display;
    private Button b1;
    private Button b2;
    private Button b3;
    private Button b4;
    private Button b5;
    private Button b6;
    private Button b7;
    private Button b8;
    private Button b9;
    private Button b0;
    private Button OpMas;
    private Button OpResta;
    private Button OpMultiplicar;
    private Button OpDividir;
    private Button OpIgual;
    private Button OpPunto;
    private Button Borrar;

    //declaracion de las variables de valor
    double numero1,numero2,resultado;
    String operador, storage = " ";
    boolean punto,bOp=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ligar los elementos a los del activity
        Display = (EditText)findViewById(R.id.Display);
        b1 = (Button)findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);
        b3 = (Button)findViewById(R.id.b3);
        b4 = (Button)findViewById(R.id.b4);
        b5 = (Button)findViewById(R.id.b5);
        b6 = (Button)findViewById(R.id.b6);
        b7 = (Button)findViewById(R.id.b7);
        b8 = (Button)findViewById(R.id.b8);
        b9 = (Button)findViewById(R.id.b9);
        b0 = (Button)findViewById(R.id.b0);
        OpMas = (Button)findViewById(R.id.OpMas);
        OpResta = (Button)findViewById(R.id.OpResta);
        OpMultiplicar = (Button)findViewById(R.id.OpMultiplicar);
        OpDividir = (Button)findViewById(R.id.OpDividir);
        OpIgual = (Button)findViewById(R.id.OpIgual);
        OpPunto = (Button)findViewById(R.id.OpPunto);
        Borrar = (Button)findViewById(R.id.Borrar);
        Display.setText("0");
        punto = true;
        operador = "n";
        //bOp=false;
    }

    //funciones de accion
    public void numeros(View b){
        String text =((Button)b).getText().toString();
        storage += text;
        Display.setText(storage);
        bOp=true;
    }
    public void Operador(View Op){
        if(bOp==true) {
            numero1 = Double.parseDouble(storage);
            storage += " " + ((Button) Op).getText().toString()+ " ";
            Display.setText(storage);
            bOp = false;
            punto = true;
        }else
            Display.setText("nop chavo");
    }
    public void reset(){
        resultado =0;
        numero1 =0;
        numero2 =0;
        storage =" ";
        bOp=true;
        punto=true;
        operador="n";
    }
    public void igual(){
        if(operador!="n"){
            switch(operador) {
                case "+": {
                    String[] parts = storage.split(" \\+");
                    numero2 = Double.parseDouble(parts[1]);
                    resultado = numero1 + numero2;
                    Display.setText(Double.toString(resultado));
                }break;
                case "-": {
                    String[] parts = storage.split(operador);
                    numero2 = Double.parseDouble(parts[1]);
                    resultado = numero1 - numero2;
                    Display.setText(Double.toString(resultado));

                }break;
                case "x": {
                    String[] parts = storage.split(operador);
                    numero2 = Double.parseDouble(parts[1]);
                    resultado = numero1 * numero2;
                    Display.setText(Double.toString(resultado));

                }break;
                case "/": {
                    String[] parts = storage.split(operador);
                    numero2 = Double.parseDouble(parts[1]);
                    resultado = numero1 / numero2;
                    Display.setText(Double.toString(resultado));

                }break;

            }
        }else{
            numero1 = Double.parseDouble(storage);
            Display.setText(storage);
        }
        reset();
    }

    //actividades Onclick
    public void cero (View view){
        numeros(view);
    }
    public void uno (View view){
        numeros(view);
    }
    public void dos (View view){
        numeros(view);
    }
    public void tres (View view){
        numeros(view);
    }
    public void cuatro (View view){
        numeros(view);
    }
    public void cinco (View view){
        numeros(view);
    }
    public void seis (View view){
        numeros(view);
    }
    public void siete (View view){
        numeros(view);
    }
    public void ocho (View view){
        numeros(view);
    }
    public void nueve (View view){
        numeros(view);
    }
    public void mas (View view){
        Operador(view);
        operador="+";
    }
    public void resta (View view){
        Operador(view);
        operador="-";
    }
    public void multiplicar (View view){
        Operador(view);
        operador="x";
    }
    public void dividir (View view){
        Operador(view);
        operador="/";
    }
    public void igual (View view){
        igual();
    }
    public void punto (View view){
        if(punto==true){
            numeros(view);
            punto=false;
        }
    }
    public void borrar (View view){
        reset();
        Display.setText("0");
    }



}
