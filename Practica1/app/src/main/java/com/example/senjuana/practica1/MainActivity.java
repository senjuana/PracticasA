package com.example.senjuana.practica1;



import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText num1, num2;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        resultado = (TextView) findViewById(R.id.Display);
    }

    public void sumar(View view){
        String valor1=num1.getText().toString();
        String valor2=num2.getText().toString();
        float nro1=Float.parseFloat(valor1);
        float nro2=Float.parseFloat(valor2);
        float suma=nro1+nro2;
        String resu=String.valueOf(suma);
        resultado.setText(resu);
    }




}
